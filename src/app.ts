import express, { Application } from 'express';
import morgan from "morgan";
import swaggerUI from "swagger-ui-express";
import swaggerJsDoc from "swagger-jsdoc";
import { swaggerconfig } from "./config/swagger.config";
import timerRoutes from './routes/time-tracker.route';
import projectRoutes from './routes/project.route';
import reportRoutes from './routes/report.route';
import { DEFAULT_PORT, PREFIX_API } from './config/const-app.config';


class App {

    public app: Application;
    private port: string;
    private apiEndpoint = {
        timeTracker : `/${PREFIX_API}/time-tracker`,
        project: `/${PREFIX_API}/project`,
        report: `/${PREFIX_API}/report`,
        swaggerUI: `/${PREFIX_API}/doc`
    };

    constructor() {
        this.app = express();
        this.port = DEFAULT_PORT;

        this.middlewares();
        this.routes();
        this.swagger();
    }


    private middlewares() {
        this.app.use(morgan("dev"));
        this.app.use(express.json());
    }

    private swagger() {
        const specs = swaggerJsDoc(swaggerconfig);
        this.app.use( this.apiEndpoint.swaggerUI , swaggerUI.serve, swaggerUI.setup(specs));
    }


    private routes() {
        this.app.use( this.apiEndpoint.project, projectRoutes );
        this.app.use( this.apiEndpoint.timeTracker, timerRoutes );
        this.app.use( this.apiEndpoint.report, reportRoutes );
    }

    listen() {
        this.app.listen( this.port, () => {
            console.info( 'Server listening at ...' + this.port );
        })
    }

}

export default App;