import { Router } from 'express';
import { getAllProjects, getProject, registerProject } from '../controllers/project.controller';


const router = Router();

/**
 * @swagger
 * components:
 *  schemas:
 *    Project:
 *      type: object
 *      properties:
 *        projectId:
 *          type: string
 *          description: project id
 *      required:
 *        - projectId
 *      example:
 *        projectId: backend-interview
 *    ProjectCreated:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *          description: message information
 *        resourse:
 *          type: string
 *          description: link resource
 *
 *  parameters:
 *    projectId:
 *      in: path
 *      name: projectId
 *      required: true
 *      schema:
 *        type: string
 *      description: The project id
 */


/**
 * @swagger
 * tags:
 *  name: Project
 *  description: Project endpoint
 */

/**
 * @swagger
 * /api/project/{projectId}:
 *  get:
 *    summary: get existing project
 *    tags: [Project]
 *    parameters:
 *      - $ref: '#/components/parameters/projectId'
 *    responses:
 *      200:
 *        description: requested project
 *        content:
 *          text/plain:
 *            schema:
 *              $ref: '#/components/schemas/Project'
 *      404:
 *        description: The project not found
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: The project does not exist
 *      400:
 *        description: Bad request
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: ProjectId is required
 *
 */
router.get('/:projectId',    getProject );


/**
 * @swagger
 * /api/project/:
 *  get:
 *    summary: get all projects
 *    tags: [Project]
 *    responses:
 *      200:
 *        description: get all projects
 *        content:
 *          text/plain:
 *            schema:
 *              $ref: '#/components/schemas/Project'
 *      404:
 *        description: The project not found
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: The project does not exist
 *
 */
router.get('',    getAllProjects );

/**
 * @swagger
 * /api/project:
 *  post:
 *    summary: Register new project
 *    tags: [Project]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Project'
 *    responses:
 *      201:
 *        description: new project registered
 *        content:
 *          text/plain:
 *            schema:
 *              $ref: '#/components/schemas/ProjectCreated'
 *      400:
 *        description: bad request
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: Project already exists
 *
 *
 */
router.post('',    registerProject );



export default router;