import { Router } from 'express';
import { endTimerForProject, startTimerForProject } from '../controllers/time-tracker.controller';

const router = Router();

/**
 * @swagger
 * components:
 *  parameters:
 *    projectId:
 *      in: path
 *      name: projectId
 *      required: true
 *      schema:
 *        type: string
 *      description: The project id
 */

/**
 * @swagger
 * tags:
 *  name: TimeTracker
 *  description: Time tracker endpoint
 */

/**
 * @swagger
 * /api/time-tracker/start/{projectId}:
 *  post:
 *    summary: starts timer for selected project
 *    tags: [TimeTracker]
 *    parameters:
 *      - $ref: '#/components/parameters/projectId'
 *    responses:
 *      200:
 *        description: timer started for selected project
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: Start tracking for project backend-interview
 *      400:
 *        description: bad request
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: Time tracker already started
 *      404:
 *        description: The project not found
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: The project does not exist
 *
 */
router.post('/start/:projectId',    startTimerForProject );


/**
 * @swagger
 * /api/time-tracker/end/{projectId}:
 *  post:
 *    summary: End timer for selected project
 *    tags: [TimeTracker]
 *    parameters:
 *      - $ref: '#/components/parameters/projectId'
 *    responses:
 *      200:
 *        description: timer ended for selected project
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: End tracking for project backend-interview. (5 minutes later)
 *      400:
 *        description: bad request
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: Time tracker not started
 *      404:
 *        description: The project not found
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: The project does not exist
 */
 router.post('/end/:projectId',    endTimerForProject );



export default router;