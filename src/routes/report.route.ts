import { Router } from 'express';
import { reportListTimesForAllProjects, reportListTimesForProject } from '../controllers/report.controller';


const router = Router();



/**
 * @swagger
 * components:
 *  schemas:
 *    ReportProject:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *          description: name report
 *        projectId:
 *          type: string
 *          description: project id
 *        totalTimeSpent:
 *          type: string
 *          description: total time spent in the project
 *        totalsegments:
 *          type: string
 *          description: total segments
 *        segments:
 *          type: string[]
 *          description: detail time spent by segment
 *      required:
 *        - ProjectId
 *      example:
 *        name: List of projects and total time for every project
 *        projectId: backend-interview
 *        totalTimeSpent: 12 minutes 35 seconds
 *        totalsegments: 2
 *        segments: [6 minutes 10 seconds, 6 minutes 25 seconds]
 *    ReportAllProjects:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *          description: name report
 *        totalProjects:
 *          type: number
 *          description: total projects tracking
 *        timeSpent:
 *          type: object[]
 *          description: detail time spent tracking
 *          properties:
 *             projectId:
 *              type: string
 *              description: project id
 *             timeSpent:
 *              type: string
 *              description: total time spent in the project
 *      example:
 *        name: Total time and list the individual time segments for a project
 *        totalProjects: 2
 *        timeSpent: [{ projectId: backend-interview, timeSpent: 6 minutes 10 seconds }, { projectId: project2, timeSpent: 6 minutes 25 seconds }]
 */


/**
 * @swagger
 * tags:
 *  name: Report
 *  description: Report endpoint
 */

/**
 * @swagger
 * /api/report/:
 *  get:
 *    summary: Report the list of projects and total time for every project
 *    tags: [Report]
 *    responses:
 *      200:
 *        description: Report the list of projects and total time for every project
 *        content:
 *          text/plain:
 *            schema:
 *              $ref: '#/components/schemas/ReportAllProjects'
 *      404:
 *        description: Report not found
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: The report does not exist
 */
router.get('',    reportListTimesForAllProjects );


/**
 * @swagger
 * /api/report/{projectId}:
 *  get:
 *    summary: Report the total time and list the individual time segments for a project
 *    tags: [Report]
 *    parameters:
 *      - $ref: '#/components/parameters/projectId'
 *    responses:
 *      200:
 *        description: Report the total time and list the individual time segments for a project
 *        content:
 *          text/plain:
 *            schema:
 *              $ref: '#/components/schemas/ReportProject'
 *      404:
 *        description: Report not found
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: The report does not exist
 *      400:
 *        description: Bad request
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: ProjectId is required
 */
router.get('/:projectId',    reportListTimesForProject );



export default router;