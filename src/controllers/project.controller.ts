import { Request, Response } from 'express';
import ProjectService from '../services/project.service';
import { DEFAULT_PATH_API, PROJECT_EXISTING, PROJECT_NOT_EXISTING, PROJECT_REQUIRED } from '../config/const-app.config';

export const registerProject = (req: Request, resp: Response) => {

    const projectId = req.body.projectId;
    const projectService = new ProjectService();

    if (!projectId) return resp.status(400).json({ message: PROJECT_REQUIRED });
    if (projectService.exists(projectId)) return resp.status(400).json({ message: PROJECT_EXISTING });
    projectService.add(projectId);
    resp.status(201).json({ message: 'Successful registration ' + projectId, resourse: DEFAULT_PATH_API + '/project/' + projectId });
}

export const getProject = (req: Request, resp: Response) => {

    const projectId = req.params.projectId;
    if (!projectId) return resp.status(400).json({ message: PROJECT_REQUIRED });
    const project = new ProjectService().get(projectId);
    if (!project) return resp.status(404).json({ message: PROJECT_NOT_EXISTING });
    resp.status(200).json(project);
}

export const getAllProjects = (req: Request, resp: Response) => {
    const project = new ProjectService().getAll();
    if (!project) return resp.status(404).json({ message: PROJECT_NOT_EXISTING });
    resp.status(200).json(project);
}





