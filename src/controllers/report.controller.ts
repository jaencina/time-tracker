import { Request, Response } from "express";
import { ReportAllProjectsModel } from "../models/report-for-all-projects.model";
import ProjectService from "../services/project.service";
import { ReportService } from "../services/report.service";



export const reportListTimesForAllProjects = (req: Request, resp: Response) => {

    const report = new ReportService().reportListTimesForAllProjects();
    if (report === {} as ReportAllProjectsModel)  return resp.status(404).json({ message: "The report does not exist" });

    resp.json(report);
};

export const reportListTimesForProject = (req: Request, resp: Response) => {
    const projectId = req.params.projectId;
    if (!new ProjectService().exists(projectId))  return resp.status(404).json({ message: "The project does not exist" });


    const report = new ReportService().reportListTimesForProject(projectId);
    if (!report.projectId)  return resp.status(404).json({ message: "The report does not exist" });

    resp.json(report);
};
