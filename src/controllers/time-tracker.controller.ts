import  { Request, Response } from 'express';
import { PROJECT_NOT_EXISTING, TIME_TRACKER_ALREADY_STARTED, TIME_TRACKER_NOT_STARTED } from '../config/const-app.config';
import ProjectService from '../services/project.service';
import { TimeTrackerService } from '../services/time-tracker.service';


export const startTimerForProject = (req: Request, resp: Response ) => {

    const projectId = req.params.projectId;
    if (!new ProjectService().exists(projectId)) return resp.status(404).json({message: PROJECT_NOT_EXISTING });

    const timeTrackerService = new TimeTrackerService();
    if (timeTrackerService.hasTimerNotEnded(projectId)) return resp.status(400).json({message: TIME_TRACKER_ALREADY_STARTED });
    timeTrackerService.startTimerTracking(projectId);

    resp.json({ message: `Start tracking for project: ${projectId}` });
}


export const endTimerForProject = (req: Request, resp: Response ) => {

    const projectId = req.params.projectId;

    if (!new ProjectService().exists(projectId)) return resp.status(404).json({message: PROJECT_NOT_EXISTING });

    const result = new TimeTrackerService().endTimerTracking(projectId);

    if (!result.success) return resp.status(400).json({message: TIME_TRACKER_NOT_STARTED });

   resp.json({ message: `End tracking for project : ${projectId}. ( ${result.timerEnd } later )` });

}



