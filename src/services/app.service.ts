import ContextService from "./context.service";


class AppService {

    protected contextService: ContextService;

    constructor() {
        this.contextService = new ContextService();
    }
}

export default AppService;