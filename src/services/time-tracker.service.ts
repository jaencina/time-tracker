import { TimeTrackerModel } from "../models/time-tracker.model";
import AppService from "./app.service";
import { nanoid } from "nanoid";

export class TimeTrackerService extends AppService {


    /**
     * start time tracker
     * @param projectId  project id to be time tracked
     */
    startTimerTracking(projectId: string) {

        const newRecord: TimeTrackerModel = {
            id: nanoid(),
            projectId,
            startTimer: new Date(),
            endTimer: undefined,
        };

        this.contextService.addTimeTracker(newRecord);
    }


    /**
     * end time tracker
     * @param projectId
     * @returns response notifying if the timer could be finished
     */
    endTimerTracking(projectId: string): { success: boolean, timerEnd?: string } {
        const timerNotEnded = this.contextService.getTimerNotEnded(projectId);

        if (!timerNotEnded) return { success: false };

        timerNotEnded.endTimer = new Date();
        this.contextService.updateTimeTracker(timerNotEnded);

        timerNotEnded.startTimer = new Date(timerNotEnded.startTimer);

        return { success: true, timerEnd : this.secondsToHmsFormat(this.diff_seconds(timerNotEnded.endTimer, timerNotEnded.startTimer)) };
    }

    /**
     * get timer not ended
     * @param projectId
     * @returns timer not ended
     */
    hasTimerNotEnded(projectId: string): boolean {
        return this.contextService.getTimerNotEnded(projectId) !== undefined;
    }

    /**
     * convert seconds to hours minutes seconds
     * @param sec seconds
     * @returns new formatted seconds
     */
    secondsToHmsFormat(sec: number): string {
        const hours = Math.floor(sec / 3600);
        const minutes = Math.floor((sec - hours * 3600) / 60);
        const seconds = sec - hours * 3600 - minutes * 60;

        const hDisplay = hours > 0 ? hours + (hours === 1 ? " hour, " : " hours, ") : "";
        const mDisplay = minutes > 0 ? minutes + (minutes === 1 ? " minute, " : " minutes, ") : "";
        const sDisplay = seconds > 0 ? seconds + (seconds === 1 ? " second" : " seconds") : "";

        return hDisplay + mDisplay + sDisplay;
      }

    /**
     * calculates the difference in seconds between date d2 and date d1
     * @param dt2
     * @param dt1
     * @returns difference in seconds between date d2 and date d1
     */
    diff_seconds(dt2: Date, dt1: Date): number {
        const diff =(dt2.getTime() - dt1.getTime()) / 1000;
        return Math.abs(Math.round(diff));
    }

}