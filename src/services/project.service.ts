import { ProjectModel } from "../models/project.model";
import AppService from "./app.service";


class ProjectService extends AppService {

    add(projectId: string) {
        this.contextService.addProject( { projectId } as ProjectModel);
    }

    exists(projectId: string): boolean {
        return this.contextService.existProject(projectId);
    }

    get(projectId: string): ProjectModel | undefined {
        return this.contextService.getProject(projectId);
    }

    getAll(): ProjectModel[] {
        return this.contextService.getAllProjects();
    }



}

export default ProjectService;