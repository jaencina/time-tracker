import { TimeTrackerModel } from "../models/time-tracker.model";
import * as fs from 'fs';
import { ContextModel } from "../models/context.model";
import { ProjectModel } from '../models/project.model';
import { DBPATH } from '../config/const-app.config';

class ContextService  {

//#region handle data consistency

    /**
     * get all records from db to file
     * @returns all records in file db.json
     */
    getRecord() : ContextModel {
        const scan = fs.readFileSync(DBPATH);
        return scan.toString().trim() === '' ?   this.getEmpyRecord() : JSON.parse(scan.toString());
    }

    /**
     * get empy model contextModel
     * @returns empy record
     */
    getEmpyRecord(): ContextModel {
        return { post: { projects: [], timeTrackers: [] } };
    }

    /**
     * update record in db (file db.json)
     * @param record model updated
     */
    updateRecord(record: ContextModel) {
        const json = JSON.stringify(record);
        fs.writeFileSync(DBPATH, json );
    }

    /**
     * clear record in file db.json
     */
    clearRecord() {
        this.updateRecord(this.getEmpyRecord());
    }

//#endregion handle data consistency

//#region project methods

    /**
     * add new project
     * @param project project model to regiter in db.json
     */
    addProject(project: ProjectModel) {
        const record = this.getRecord();
        record.post.projects.push(project);
        this.updateRecord(record);
    }

    /**
     * get project from db.json
     * @param projectId project id
     * @return requested project
     */
    getProject(projectId: string): ProjectModel | undefined {
        return this.getRecord().post.projects.find(x => x.projectId === projectId);
    }

    /**
     * get all projects from db.json
     * @returns all projects
     */
    getAllProjects(): ProjectModel[] {
        return this.getRecord().post.projects;
    }

    /**
     * check if project exists in db
     * @param projectId id project
     * @returns if the project exists it returns true, if it does not exist false
     */
    existProject(projectId: string): boolean {
        return this.getRecord().post.projects.some(x => x.projectId === projectId);
    }

//#endregion project methods

//#region timeTracker methods

    /**
     * add new time tracker
     * @param timeTracker timeTracker model to add
     */
    addTimeTracker(timeTracker: TimeTrackerModel) {
        const record = this.getRecord();
        record.post.timeTrackers.push(timeTracker);
        this.updateRecord(record);
    }


    /**
     * update time tracker
     * @param timeTracker timeTracker model to update
     */
    updateTimeTracker(timeTracker: TimeTrackerModel) {
        const record = this.getRecord();
        const index = record.post.timeTrackers.findIndex(x => x.id === timeTracker.id);
        record.post.timeTrackers[index] = timeTracker;
        this.updateRecord(record);
    }

    /**
     * get timer not ended
     * @param proyectId project id
     * @returns model timer not ended
     */
    getTimerNotEnded(proyectId: string): TimeTrackerModel | undefined {
        const record = this.getRecord();
        return record.post.timeTrackers.find(x => x.projectId === proyectId && x.endTimer === undefined);
    }

    /**
     * get all time tracked
     * @returns all time tracked
     */
    getAllTimeTracker(): TimeTrackerModel[] {
        const record = this.getRecord();
        return record.post.timeTrackers.filter(x => x.endTimer !== undefined);
    }

    /**
     * get all time traces of a project
     * @param projectId project id
     * @returns traces of project
     */
    getTimeTrackerProject(projectId: string): TimeTrackerModel[] {
        const record = this.getRecord();
        return record.post.timeTrackers.filter(x => x.projectId === projectId && x.endTimer !== undefined);
    }

//#endregion timeTracker methods



}

export default ContextService;