import { GENERAL_REPORT_NAME, ReportAllProjectsModel, TimeSpentProject } from "../models/report-for-all-projects.model";
import { ReportProjectsModel, REPORT_FOR_PROJECT_NAME } from "../models/report-project.model";
import { TimeTrackerModel, TotalSecondsProjectModel } from "../models/time-tracker.model";
import AppService from "./app.service";
import { TimeTrackerService } from "./time-tracker.service";


export class ReportService extends AppService {


    private timeTrackerService: TimeTrackerService;
    constructor() {
        super();
        this.timeTrackerService = new TimeTrackerService();
    }

    /**
     * Report the list of projects and total time for every project
     * @returns list of projects and total time for every project
     */
    reportListTimesForAllProjects(): ReportAllProjectsModel {
        const report: ReportAllProjectsModel = {} as ReportAllProjectsModel;
        report.totalProjects = 0;
        report.timeSpent = [];

        const timeTracker = this.contextService.getAllTimeTracker();

        if (timeTracker.length === 0) return report;

        report.name = GENERAL_REPORT_NAME;
        const registerSecondsProjects: TotalSecondsProjectModel[] = [];
        timeTracker.forEach(element => {
            element.startTimer = new Date(element.startTimer);
            element.endTimer = new Date(element.endTimer as Date);

            const totalSeconds = this.timeTrackerService.diff_seconds(element.endTimer as Date, element.startTimer);
            registerSecondsProjects.push({ projectId:element.projectId, seconds: totalSeconds });
        });


        const projectIds = this.getuniqueProjectId(timeTracker);
        report.totalProjects = projectIds.length;

        projectIds.forEach(projectId => {

            const totalSecondsProject = registerSecondsProjects.filter(x => x.projectId === projectId).reduce((a, b) => (typeof b.seconds === 'number' ? a + b.seconds : a), 0);

            report.timeSpent.push({
                projectId,
                timeSpent: this.timeTrackerService.secondsToHmsFormat(totalSecondsProject),
            });
        });

        return report;
    }


    /**
     * Report the total time and list the individual time segments for a project
     * @param projectId project id
     * @returns total time and list the individual time segments for a project
     */
    reportListTimesForProject(projectId: string): ReportProjectsModel {
        const report: ReportProjectsModel = {} as ReportProjectsModel;
        report.segments = [];

        const timesForProject = this.contextService.getTimeTrackerProject(projectId);

        if (timesForProject.length === 0) return report;

        report.name = REPORT_FOR_PROJECT_NAME;
        report.projectId = timesForProject[0].projectId;
        report.totalsegments = timesForProject.length;

        let totalSeconds = 0;
        timesForProject.forEach(element => {

            element.startTimer = new Date(element.startTimer);
            element.endTimer = new Date(element.endTimer as Date);

            const secondsTimer = this.timeTrackerService.diff_seconds(element.endTimer as Date, element.startTimer);
            totalSeconds += secondsTimer;

            report.segments.push(this.timeTrackerService.secondsToHmsFormat(secondsTimer));
        });


        report.totalTimeSpent = this.timeTrackerService.secondsToHmsFormat(totalSeconds);

        return report;

    }


    /**
     * get unique values ​​
     * @param timerTracker
     * @returns
     */
    private getuniqueProjectId(timerTracker: TimeTrackerModel[]) {
        const result: string[] = [];
        timerTracker.forEach( (elemento) => {
          if (!result.includes(elemento.projectId)) {
            result.push(elemento.projectId);
          }
        });

        return result;
    }

}