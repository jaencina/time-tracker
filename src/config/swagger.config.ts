import { DEFAULT_PORT, HOST } from "./const-app.config";

export const swaggerconfig = {
    definition: {
      openapi: "3.0.0",
      info: {
        title: "Timer Tracker API",
        version: "1.0.0",
        description: "Timer tracker projects API",
        contact: {
            email: "hector.encina.escobar@gmail.com"
        },
      },
      servers: [
        {
          url: HOST + ":" +  DEFAULT_PORT,
        },
      ],
    },
    apis: ["./src/routes/*.ts"],
  };