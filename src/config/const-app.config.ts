

export const HOST = 'http://localhost';
export const DEFAULT_PORT = process.env.PORT ?  process.env.PORT : '8001';
export const PREFIX_API = 'api'
export const DEFAULT_PATH_API = `${ HOST }:${DEFAULT_PORT}/${PREFIX_API}`;
export const DBPATH = 'src/persistence/db.json';




//#region messages aplication

export const PROJECT_EXISTING = 'Project already exists';
export const PROJECT_NOT_EXISTING = 'The project does not exist';
export const PROJECT_REQUIRED = 'ProjectId is required';
export const TIME_TRACKER_NOT_STARTED = 'Time tracker not started';
export const TIME_TRACKER_ALREADY_STARTED = 'Time tracker already started';


//#endregion