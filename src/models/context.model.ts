import { ProjectModel } from "./project.model";
import { TimeTrackerModel } from "./time-tracker.model";

export interface ContextModel {
    post: {
        projects: ProjectModel[],
        timeTrackers: TimeTrackerModel[]
    },

}