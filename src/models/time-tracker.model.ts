

export interface TimeTrackerModel {
    id: string,
    projectId: string,
    startTimer: Date,
    endTimer: Date | undefined,
}


export interface TotalSecondsProjectModel {
    projectId: string,
    seconds: number
}