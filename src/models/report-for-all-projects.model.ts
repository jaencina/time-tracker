
export const GENERAL_REPORT_NAME = 'List times for all projects';

export interface ReportAllProjectsModel {
    name: string;
    totalProjects: number;
    timeSpent: TimeSpentProject[]
}



export interface TimeSpentProject {
    projectId: string;
    timeSpent: string;
}