
export const REPORT_FOR_PROJECT_NAME = 'List times for project';

export interface ReportProjectsModel {
    name: string;
    projectId: string;
    totalTimeSpent: string
    totalsegments: number;
    segments: string[];
}