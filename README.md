# Time Tracker API 

 API rest-ful que permite iniciar y detener un temporizador para realizar un seguimiento del tiempo empleado en diferentes proyectos.
 Desarrollada con Express y Typescript.


## Caracteristicas desarrolladas

- Iniciar temporizador de un proyecto
- Detener temporizador de un proyecto
- Reporte de la lista de proyectos y el tiempo total de cada proyecto
- Reporte del tiempo total y por segmento de tiempo de un proyecto
- Registrar, consultar proyecto

## Documentación API
Para acceder a la documentación ingrese a la ruta /api/doc, una vez que levante el proyecto.

ejemplo:
```sh
http://localhost:8001/api/doc/
```
## Endpoints
registrar un nuevo proyecto:  
```sh
http://localhost:8001/api/project/
```

Iniciar temporizador:  
```sh
http://localhost:8001/api/time-tracker/start/{projectId}
```

Finalizar temporizador:  
```sh
http://localhost:8001/api/time-tracker/end/{projectId}
```

Reporte de la lista de proyectos y el tiempo total de cada proyecto:

```sh
http://localhost:8001/api/report
```

Reporte del tiempo total y por segmento de tiempo de un proyecto:

```sh
http://localhost:8001/api/report/{projectId}
```

## Instalación

Se requiere node [Node.js](https://nodejs.org/) v14 o superior.

Para instalar las dependencias necesarias y de desarrollo ejecute:

```sh
npm install
```

una vez instaladas ejecute:

```sh
npm run dev
```

Para la persistencia de datos se utilizo un archivo, por lo que no es necesaria ninguna configuración extra.

## Pruebas

Se ejecuto un plan de pruebas basado en pruebas de integración en cada endpoint. 

Dependencias utilizadas para el desarrollo de pruebas
- Jest
- Supertest

Para iniciar las pruebas ejecute:

```sh
npm test
```

## Contacto

Hector.encina.escobar@gmail.com




