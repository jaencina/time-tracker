import request from 'supertest'
import App from '../../src/app'
import { DEFAULT_PATH_API, PROJECT_NOT_EXISTING } from '../../src/config/const-app.config';
import ContextService from '../../src/services/context.service';
import  { ContextModel } from '../../src/models/context.model';

const server = new App();
const contextService = new ContextService();
let dbState: ContextModel = {} as ContextModel;

const api = request (server.app);

beforeAll(async () => {
    dbState = contextService.getRecord();
    contextService.clearRecord();
})

afterAll(async () => {
    contextService.updateRecord(dbState);
});

describe('/api/project', () => {


    it('should return 404', done => {
         api
          .get(`/api/project/backend-interview`)
          .expect('Content-Type', /json/)
          .expect(404)
          .end((err, res) => {
            if (err) return done(err)
            expect(res.body).toMatchObject({ message: PROJECT_NOT_EXISTING })
            done()
          })
      })


      it('should return 201', done => {
        api
          .post(`/api/project`)
          .send({projectId: 'backend-interview'})
          .expect('Content-Type', /json/)
          .expect(201)
          .end((err, res) => {
            if (err) return done(err)
            expect(res.body).toMatchObject({  message: 'Successful registration backend-interview', resourse: DEFAULT_PATH_API + '/project/backend-interview' })
            done()
          })
      })


  it('should return 200',   done => {
    api
      .get(`/api/project/backend-interview`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.body).toMatchObject({projectId: 'backend-interview' })
        done()
      })
  })

})