import request from 'supertest'
import App from '../../src/app'
import { DEFAULT_PATH_API, PROJECT_NOT_EXISTING, TIME_TRACKER_ALREADY_STARTED } from '../../src/config/const-app.config';
import ContextService from '../../src/services/context.service';
import  { ContextModel } from '../../src/models/context.model';
// import { TimeTrackerService } from '../../src/services/time-tracker.service';
import { ReportService } from '../../src/services/report.service';

const server = new App();
const contextService = new ContextService();
// const timeTrackerService = new TimeTrackerService();
const reportService = new ReportService();
let dbState: ContextModel = {} as ContextModel;

const api = request (server.app);

beforeAll(async () => {
    dbState = contextService.getRecord();
    contextService.clearRecord();
})

afterAll(async () => {
    contextService.updateRecord(dbState);
});

describe('api/time-tracker', () => {


    it('should return 404', done => {
         api
          .post(`/api/time-tracker/start/backend-interview`)
          .expect('Content-Type', /json/)
          .expect(404)
          .end((err, res) => {
            if (err) return done(err)
            expect(res.body).toMatchObject({ message: PROJECT_NOT_EXISTING })
            done()
          })
      })


      it('should return 201', done => {
        api
          .post(`/api/project`)
          .send({projectId: 'backend-interview'})
          .expect('Content-Type', /json/)
          .expect(201)
          .end((err, res) => {
            if (err) return done(err)
            expect(res.body).toMatchObject({  message: 'Successful registration backend-interview', resourse: DEFAULT_PATH_API + '/project/backend-interview' })
            done()
          })
      })


      it('should return 200', done => {
        api
         .post(`/api/time-tracker/start/backend-interview`)
         .expect('Content-Type', /json/)
          .expect(200)
          .end((err, res) => {
            if (err) return done(err)
            expect(res.body).toMatchObject({ message: `Start tracking for project: backend-interview` })
            done()
          })
      })

      it('should return 400', done => {
        api
         .post(`/api/time-tracker/start/backend-interview`)
         .expect('Content-Type', /json/)
          .expect(400)
          .end((err, res) => {
            if (err) return done(err)
            expect(res.body).toMatchObject({message: TIME_TRACKER_ALREADY_STARTED })
            done()
          })
      })

      it('should return 200', done => {
        api
         .post(`/api/time-tracker/end/backend-interview`)
         .expect('Content-Type', /json/)
          .expect(200)
          .end((err, res) => {
            if (err) return done(err)


            const timerFormated = reportService.reportListTimesForProject('backend-interview');

            expect(res.body).toMatchObject({ message: `End tracking for project : backend-interview. ( ${timerFormated.totalTimeSpent } later )` })
            done()
          })
      })




})